from antlr4 import *
from jackLexer import jackLexer
from jackListener import jackListener
from jackParser import jackParser
import os


class JackAnalyzer(jackListener):
    symbols = ['{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&', '|', '<', '>', '=', '~']
    keywords = ['class', 'constructor', 'function', 'method', 'field', 'static', 'var', 'int', 'char', 'boolean',
                'void', 'true', 'false', 'null', 'this', 'let', 'do', 'if', 'else', 'while', 'return']

    def __init__(self, file):
        self.__f = file

    # Enter a parse tree produced by jackParser#classStructure.
    def enterClassStructure(self, ctx: jackParser.ClassStructureContext):
        self.__f.write('<class>\n')

    # Exit a parse tree produced by jackParser#classStructure.
    def exitClassStructure(self, ctx: jackParser.ClassStructureContext):
        self.__f.write('</class>\n')

    # Enter a parse tree produced by jackParser#classVarDec.
    def enterClassVarDec(self, ctx: jackParser.ClassVarDecContext):
        self.__f.write('<classVarDec>\n')

    # Exit a parse tree produced by jackParser#classVarDec.
    def exitClassVarDec(self, ctx: jackParser.ClassVarDecContext):
        self.__f.write('</classVarDec>\n')

    # Enter a parse tree produced by jackParser#varType.
    def enterVarType(self, ctx: jackParser.VarTypeContext):
        pass

    # Exit a parse tree produced by jackParser#varType.
    def exitVarType(self, ctx: jackParser.VarTypeContext):
        pass

    # Enter a parse tree produced by jackParser#subroutineDec.
    def enterSubroutineDec(self, ctx: jackParser.SubroutineDecContext):
        self.__f.write('<subroutineDec>\n')

    # Exit a parse tree produced by jackParser#subroutineDec.
    def exitSubroutineDec(self, ctx: jackParser.SubroutineDecContext):
        self.__f.write('</subroutineDec>\n')

    # Enter a parse tree produced by jackParser#parameterList.
    def enterParameterList(self, ctx: jackParser.ParameterListContext):
        self.__f.write('<parameterList>\n')

    # Exit a parse tree produced by jackParser#parameterList.
    def exitParameterList(self, ctx: jackParser.ParameterListContext):
        self.__f.write('</parameterList>\n')

    # Enter a parse tree produced by jackParser#subroutineBody.
    def enterSubroutineBody(self, ctx: jackParser.SubroutineBodyContext):
        self.__f.write('<subroutineBody>\n')

    # Exit a parse tree produced by jackParser#subroutineBody.
    def exitSubroutineBody(self, ctx: jackParser.SubroutineBodyContext):
        self.__f.write('</subroutineBody>\n')

    # Enter a parse tree produced by jackParser#varDec.
    def enterVarDec(self, ctx: jackParser.VarDecContext):
        self.__f.write('<varDec>\n')

    # Exit a parse tree produced by jackParser#varDec.
    def exitVarDec(self, ctx: jackParser.VarDecContext):
        self.__f.write('</varDec>\n')

    # Enter a parse tree produced by jackParser#className.
    def enterClassName(self, ctx: jackParser.ClassNameContext):
        self.__f.write('<identifier> ' + str(ctx.children[0]) + ' </identifier>\n')

    # Exit a parse tree produced by jackParser#className.
    def exitClassName(self, ctx: jackParser.ClassNameContext):
        pass

    # Enter a parse tree produced by jackParser#subroutineName.
    def enterSubroutineName(self, ctx: jackParser.SubroutineNameContext):
        self.__f.write('<identifier>' + str(ctx.children[0]) + ' </identifier>\n')

    # Exit a parse tree produced by jackParser#subroutineName.
    def exitSubroutineName(self, ctx: jackParser.SubroutineNameContext):
        pass

    # Enter a parse tree produced by jackParser#varName.
    def enterVarName(self, ctx: jackParser.VarNameContext):
        self.__f.write('<identifier> ' + str(ctx.children[0]) + ' </identifier>\n')

    # Exit a parse tree produced by jackParser#varName.
    def exitVarName(self, ctx: jackParser.VarNameContext):
        pass

    # Enter a parse tree produced by jackParser#statements.
    def enterStatements(self, ctx: jackParser.StatementsContext):
        self.__f.write('<statements>\n')

    # Exit a parse tree produced by jackParser#statements.
    def exitStatements(self, ctx: jackParser.StatementsContext):
        self.__f.write('</statements>\n')

    # Enter a parse tree produced by jackParser#statement.
    def enterStatement(self, ctx: jackParser.StatementContext):
        pass

    # Exit a parse tree produced by jackParser#statement.
    def exitStatement(self, ctx: jackParser.StatementContext):
        pass

    # Enter a parse tree produced by jackParser#letStatement.
    def enterLetStatement(self, ctx: jackParser.LetStatementContext):
        self.__f.write('<letStatement>\n')

    # Exit a parse tree produced by jackParser#letStatement.
    def exitLetStatement(self, ctx: jackParser.LetStatementContext):
        self.__f.write('</letStatement>\n')

    # Enter a parse tree produced by jackParser#ifStatement.
    def enterIfStatement(self, ctx: jackParser.IfStatementContext):
        self.__f.write('<ifStatement>\n')

    # Exit a parse tree produced by jackParser#ifStatement.
    def exitIfStatement(self, ctx: jackParser.IfStatementContext):
        self.__f.write('</ifStatement>\n')

    # Enter a parse tree produced by jackParser#whileStatement.
    def enterWhileStatement(self, ctx: jackParser.WhileStatementContext):
        self.__f.write('<whileStatement>\n')

    # Exit a parse tree produced by jackParser#whileStatement.
    def exitWhileStatement(self, ctx: jackParser.WhileStatementContext):
        self.__f.write('</whileStatement>\n')

    # Enter a parse tree produced by jackParser#doStatement.
    def enterDoStatement(self, ctx: jackParser.DoStatementContext):
        self.__f.write('<doStatement>\n')

    # Exit a parse tree produced by jackParser#doStatement.
    def exitDoStatement(self, ctx: jackParser.DoStatementContext):
        self.__f.write('</doStatement>\n')

    # Enter a parse tree produced by jackParser#returnStatement.
    def enterReturnStatement(self, ctx: jackParser.ReturnStatementContext):
        self.__f.write('<returnStatement>\n')

    # Exit a parse tree produced by jackParser#returnStatement.
    def exitReturnStatement(self, ctx: jackParser.ReturnStatementContext):
        self.__f.write('</returnStatement>\n')

    # Enter a parse tree produced by jackParser#expression.
    def enterExpression(self, ctx: jackParser.ExpressionContext):
        self.__f.write('<expression>\n')

    # Exit a parse tree produced by jackParser#expression.
    def exitExpression(self, ctx: jackParser.ExpressionContext):
        self.__f.write('</expression>\n')

    # Enter a parse tree produced by jackParser#term.
    def enterTerm(self, ctx: jackParser.TermContext):
        self.__f.write('<term>\n')

        if '"' in str(ctx.children[0]):
            self.__f.write('<stringConstant> ' + str(ctx.children[0]).replace('"', '') + ' </stringConstant>\n')
        elif str(ctx.children[0]).isnumeric():
            self.__f.write('<integerConstant> ' + str(ctx.children[0]) + ' </integerConstant>\n')

    # Exit a parse tree produced by jackParser#term.
    def exitTerm(self, ctx: jackParser.TermContext):
        self.__f.write('</term>\n')

    # Enter a parse tree produced by jackParser#subroutineCall.
    def enterSubroutineCall(self, ctx: jackParser.SubroutineCallContext):
        pass

    # Exit a parse tree produced by jackParser#subroutineCall.
    def exitSubroutineCall(self, ctx: jackParser.SubroutineCallContext):
        pass

    # Enter a parse tree produced by jackParser#expressionList.
    def enterExpressionList(self, ctx: jackParser.ExpressionListContext):
        self.__f.write('<expressionList>\n')

    # Exit a parse tree produced by jackParser#expressionList.
    def exitExpressionList(self, ctx: jackParser.ExpressionListContext):
        self.__f.write('</expressionList>\n')

    # Enter a parse tree produced by jackParser#op.
    def enterOp(self, ctx: jackParser.OpContext):
        pass

    # Exit a parse tree produced by jackParser#op.
    def exitOp(self, ctx: jackParser.OpContext):
        pass

    # Enter a parse tree produced by jackParser#keywordConstant.
    def enterKeywordConstant(self, ctx: jackParser.KeywordConstantContext):
        pass

    # Exit a parse tree produced by jackParser#keywordConstant.
    def exitKeywordConstant(self, ctx: jackParser.KeywordConstantContext):
        pass

    # Enter a parse tree produced by jackParser#unaryOp.
    def enterUnaryOp(self, ctx: jackParser.UnaryOpContext):
        pass

    # Exit a parse tree produced by jackParser#unaryOp.
    def exitUnaryOp(self, ctx: jackParser.UnaryOpContext):
        pass

    def visitTerminal(self, node: TerminalNode):
        if str(node) in self.symbols:
            symbol = str(node)

            if symbol == '<':
                symbol = '&lt;'
            elif symbol == '>':
                symbol = '&gt;'
            elif symbol == '&':
                symbol = '&amp;'

            self.__f.write('<symbol> ' + symbol + ' </symbol>\n')
        elif str(node) in self.keywords:
            self.__f.write('<keyword> ' + str(node) + ' </keyword>\n')


def main():
    dir = r'path_to_jack_program'

    is_file = os.path.isfile(dir)
    is_dir = os.path.isdir(dir)

    inputs = []

    if is_file:
        inputs.append(FileStream(dir))
    elif is_dir:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if f.endswith('.jack'):
                inputs.append(FileStream(os.path.join(dir, f)))

    for input in inputs:
        f = open(input.fileName.replace('.jack', '.xml'), "w")

        lexer = jackLexer(input)
        stream = CommonTokenStream(lexer)
        parser = jackParser(stream)

        tree = parser.classStructure()
        printer = JackAnalyzer(f)
        walker = ParseTreeWalker()
        walker.walk(printer, tree)


if __name__ == '__main__':
    main()