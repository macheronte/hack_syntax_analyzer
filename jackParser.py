# Generated from .\jack.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\60")
        buf.write("\u0102\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\3\2\3\2\3\2\3\2\7\2\67\n\2\f\2\16\2:\13\2\3\2\7")
        buf.write("\2=\n\2\f\2\16\2@\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3")
        buf.write("I\n\3\f\3\16\3L\13\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4T\n\4")
        buf.write("\3\5\3\5\3\5\5\5Y\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6")
        buf.write("\3\6\3\6\3\6\3\6\3\6\7\6h\n\6\f\6\16\6k\13\6\5\6m\n\6")
        buf.write("\3\7\3\7\7\7q\n\7\f\7\16\7t\13\7\3\7\3\7\3\7\3\b\3\b\3")
        buf.write("\b\3\b\3\b\7\b~\n\b\f\b\16\b\u0081\13\b\3\b\3\b\3\t\3")
        buf.write("\t\3\n\3\n\3\13\3\13\3\f\7\f\u008c\n\f\f\f\16\f\u008f")
        buf.write("\13\f\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u0097\n\r\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\5\16\u009f\n\16\3\16\3\16\3\16\3")
        buf.write("\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\5\17\u00b1\n\17\3\20\3\20\3\20\3\20\3\20\3")
        buf.write("\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\5\22\u00c1")
        buf.write("\n\22\3\22\3\22\3\23\3\23\3\23\3\23\7\23\u00c9\n\23\f")
        buf.write("\23\16\23\u00cc\13\23\3\24\3\24\3\24\3\24\3\24\3\24\3")
        buf.write("\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24")
        buf.write("\5\24\u00df\n\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5")
        buf.write("\25\u00e8\n\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u00f0")
        buf.write("\n\25\3\26\3\26\3\26\7\26\u00f5\n\26\f\26\16\26\u00f8")
        buf.write("\13\26\5\26\u00fa\n\26\3\27\3\27\3\30\3\30\3\31\3\31\3")
        buf.write("\31\2\2\32\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$")
        buf.write("&(*,.\60\2\7\3\2\6\7\3\2\r\17\4\2\27\27\36%\3\2&)\4\2")
        buf.write("\37\37**\2\u0109\2\62\3\2\2\2\4C\3\2\2\2\6S\3\2\2\2\b")
        buf.write("U\3\2\2\2\nl\3\2\2\2\fn\3\2\2\2\16x\3\2\2\2\20\u0084\3")
        buf.write("\2\2\2\22\u0086\3\2\2\2\24\u0088\3\2\2\2\26\u008d\3\2")
        buf.write("\2\2\30\u0096\3\2\2\2\32\u0098\3\2\2\2\34\u00a4\3\2\2")
        buf.write("\2\36\u00b2\3\2\2\2 \u00ba\3\2\2\2\"\u00be\3\2\2\2$\u00c4")
        buf.write("\3\2\2\2&\u00de\3\2\2\2(\u00ef\3\2\2\2*\u00f9\3\2\2\2")
        buf.write(",\u00fb\3\2\2\2.\u00fd\3\2\2\2\60\u00ff\3\2\2\2\62\63")
        buf.write("\7\3\2\2\63\64\5\20\t\2\648\7\4\2\2\65\67\5\4\3\2\66\65")
        buf.write("\3\2\2\2\67:\3\2\2\28\66\3\2\2\289\3\2\2\29>\3\2\2\2:")
        buf.write("8\3\2\2\2;=\5\b\5\2<;\3\2\2\2=@\3\2\2\2><\3\2\2\2>?\3")
        buf.write("\2\2\2?A\3\2\2\2@>\3\2\2\2AB\7\5\2\2B\3\3\2\2\2CD\t\2")
        buf.write("\2\2DE\5\6\4\2EJ\5\24\13\2FG\7\b\2\2GI\5\24\13\2HF\3\2")
        buf.write("\2\2IL\3\2\2\2JH\3\2\2\2JK\3\2\2\2KM\3\2\2\2LJ\3\2\2\2")
        buf.write("MN\7\t\2\2N\5\3\2\2\2OT\7\n\2\2PT\7\13\2\2QT\7\f\2\2R")
        buf.write("T\5\20\t\2SO\3\2\2\2SP\3\2\2\2SQ\3\2\2\2SR\3\2\2\2T\7")
        buf.write("\3\2\2\2UX\t\3\2\2VY\7\20\2\2WY\5\6\4\2XV\3\2\2\2XW\3")
        buf.write("\2\2\2YZ\3\2\2\2Z[\5\22\n\2[\\\7\21\2\2\\]\5\n\6\2]^\7")
        buf.write("\22\2\2^_\5\f\7\2_\t\3\2\2\2`a\5\6\4\2ab\5\24\13\2bi\3")
        buf.write("\2\2\2cd\7\b\2\2de\5\6\4\2ef\5\24\13\2fh\3\2\2\2gc\3\2")
        buf.write("\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2jm\3\2\2\2ki\3\2\2\2")
        buf.write("l`\3\2\2\2lm\3\2\2\2m\13\3\2\2\2nr\7\4\2\2oq\5\16\b\2")
        buf.write("po\3\2\2\2qt\3\2\2\2rp\3\2\2\2rs\3\2\2\2su\3\2\2\2tr\3")
        buf.write("\2\2\2uv\5\26\f\2vw\7\5\2\2w\r\3\2\2\2xy\7\23\2\2yz\5")
        buf.write("\6\4\2z\177\5\24\13\2{|\7\b\2\2|~\5\24\13\2}{\3\2\2\2")
        buf.write("~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082")
        buf.write("\3\2\2\2\u0081\177\3\2\2\2\u0082\u0083\7\t\2\2\u0083\17")
        buf.write("\3\2\2\2\u0084\u0085\7-\2\2\u0085\21\3\2\2\2\u0086\u0087")
        buf.write("\7-\2\2\u0087\23\3\2\2\2\u0088\u0089\7-\2\2\u0089\25\3")
        buf.write("\2\2\2\u008a\u008c\5\30\r\2\u008b\u008a\3\2\2\2\u008c")
        buf.write("\u008f\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008e\3\2\2\2")
        buf.write("\u008e\27\3\2\2\2\u008f\u008d\3\2\2\2\u0090\u0097\5\32")
        buf.write("\16\2\u0091\u0097\5\34\17\2\u0092\u0097\5\36\20\2\u0093")
        buf.write("\u0097\5 \21\2\u0094\u0097\5\"\22\2\u0095\u0097\5\16\b")
        buf.write("\2\u0096\u0090\3\2\2\2\u0096\u0091\3\2\2\2\u0096\u0092")
        buf.write("\3\2\2\2\u0096\u0093\3\2\2\2\u0096\u0094\3\2\2\2\u0096")
        buf.write("\u0095\3\2\2\2\u0097\31\3\2\2\2\u0098\u0099\7\24\2\2\u0099")
        buf.write("\u009e\5\24\13\2\u009a\u009b\7\25\2\2\u009b\u009c\5$\23")
        buf.write("\2\u009c\u009d\7\26\2\2\u009d\u009f\3\2\2\2\u009e\u009a")
        buf.write("\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0")
        buf.write("\u00a1\7\27\2\2\u00a1\u00a2\5$\23\2\u00a2\u00a3\7\t\2")
        buf.write("\2\u00a3\33\3\2\2\2\u00a4\u00a5\7\30\2\2\u00a5\u00a6\7")
        buf.write("\21\2\2\u00a6\u00a7\5$\23\2\u00a7\u00a8\7\22\2\2\u00a8")
        buf.write("\u00a9\7\4\2\2\u00a9\u00aa\5\26\f\2\u00aa\u00b0\7\5\2")
        buf.write("\2\u00ab\u00ac\7\31\2\2\u00ac\u00ad\7\4\2\2\u00ad\u00ae")
        buf.write("\5\26\f\2\u00ae\u00af\7\5\2\2\u00af\u00b1\3\2\2\2\u00b0")
        buf.write("\u00ab\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\35\3\2\2\2\u00b2")
        buf.write("\u00b3\7\32\2\2\u00b3\u00b4\7\21\2\2\u00b4\u00b5\5$\23")
        buf.write("\2\u00b5\u00b6\7\22\2\2\u00b6\u00b7\7\4\2\2\u00b7\u00b8")
        buf.write("\5\26\f\2\u00b8\u00b9\7\5\2\2\u00b9\37\3\2\2\2\u00ba\u00bb")
        buf.write("\7\33\2\2\u00bb\u00bc\5(\25\2\u00bc\u00bd\7\t\2\2\u00bd")
        buf.write("!\3\2\2\2\u00be\u00c0\7\34\2\2\u00bf\u00c1\5$\23\2\u00c0")
        buf.write("\u00bf\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\u00c2\3\2\2\2")
        buf.write("\u00c2\u00c3\7\t\2\2\u00c3#\3\2\2\2\u00c4\u00ca\5&\24")
        buf.write("\2\u00c5\u00c6\5,\27\2\u00c6\u00c7\5&\24\2\u00c7\u00c9")
        buf.write("\3\2\2\2\u00c8\u00c5\3\2\2\2\u00c9\u00cc\3\2\2\2\u00ca")
        buf.write("\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb%\3\2\2\2\u00cc")
        buf.write("\u00ca\3\2\2\2\u00cd\u00df\7+\2\2\u00ce\u00df\7,\2\2\u00cf")
        buf.write("\u00df\5.\30\2\u00d0\u00df\5\24\13\2\u00d1\u00d2\5\24")
        buf.write("\13\2\u00d2\u00d3\7\25\2\2\u00d3\u00d4\5$\23\2\u00d4\u00d5")
        buf.write("\7\26\2\2\u00d5\u00df\3\2\2\2\u00d6\u00df\5(\25\2\u00d7")
        buf.write("\u00d8\7\21\2\2\u00d8\u00d9\5$\23\2\u00d9\u00da\7\22\2")
        buf.write("\2\u00da\u00df\3\2\2\2\u00db\u00dc\5\60\31\2\u00dc\u00dd")
        buf.write("\5&\24\2\u00dd\u00df\3\2\2\2\u00de\u00cd\3\2\2\2\u00de")
        buf.write("\u00ce\3\2\2\2\u00de\u00cf\3\2\2\2\u00de\u00d0\3\2\2\2")
        buf.write("\u00de\u00d1\3\2\2\2\u00de\u00d6\3\2\2\2\u00de\u00d7\3")
        buf.write("\2\2\2\u00de\u00db\3\2\2\2\u00df\'\3\2\2\2\u00e0\u00e1")
        buf.write("\5\22\n\2\u00e1\u00e2\7\21\2\2\u00e2\u00e3\5*\26\2\u00e3")
        buf.write("\u00e4\7\22\2\2\u00e4\u00f0\3\2\2\2\u00e5\u00e8\5\20\t")
        buf.write("\2\u00e6\u00e8\5\24\13\2\u00e7\u00e5\3\2\2\2\u00e7\u00e6")
        buf.write("\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ea\7\35\2\2\u00ea")
        buf.write("\u00eb\5\22\n\2\u00eb\u00ec\7\21\2\2\u00ec\u00ed\5*\26")
        buf.write("\2\u00ed\u00ee\7\22\2\2\u00ee\u00f0\3\2\2\2\u00ef\u00e0")
        buf.write("\3\2\2\2\u00ef\u00e7\3\2\2\2\u00f0)\3\2\2\2\u00f1\u00f6")
        buf.write("\5$\23\2\u00f2\u00f3\7\b\2\2\u00f3\u00f5\5$\23\2\u00f4")
        buf.write("\u00f2\3\2\2\2\u00f5\u00f8\3\2\2\2\u00f6\u00f4\3\2\2\2")
        buf.write("\u00f6\u00f7\3\2\2\2\u00f7\u00fa\3\2\2\2\u00f8\u00f6\3")
        buf.write("\2\2\2\u00f9\u00f1\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa+")
        buf.write("\3\2\2\2\u00fb\u00fc\t\4\2\2\u00fc-\3\2\2\2\u00fd\u00fe")
        buf.write("\t\5\2\2\u00fe/\3\2\2\2\u00ff\u0100\t\6\2\2\u0100\61\3")
        buf.write("\2\2\2\268>JSXilr\177\u008d\u0096\u009e\u00b0\u00c0\u00ca")
        buf.write("\u00de\u00e7\u00ef\u00f6\u00f9")
        return buf.getvalue()


class jackParser ( Parser ):

    grammarFileName = "jack.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'class'", "'{'", "'}'", "'static'", "'field'", 
                     "','", "';'", "'int'", "'char'", "'boolean'", "'constructor'", 
                     "'function'", "'method'", "'void'", "'('", "')'", "'var'", 
                     "'let'", "'['", "']'", "'='", "'if'", "'else'", "'while'", 
                     "'do'", "'return'", "'.'", "'+'", "'-'", "'*'", "'/'", 
                     "'&'", "'|'", "'<'", "'>'", "'true'", "'false'", "'null'", 
                     "'this'", "'~'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "INTEGERCONSTANT", "STRINGCONSTANT", 
                      "IDENTIFIER", "WS", "COMMENT", "LINE_COMMENT" ]

    RULE_classStructure = 0
    RULE_classVarDec = 1
    RULE_varType = 2
    RULE_subroutineDec = 3
    RULE_parameterList = 4
    RULE_subroutineBody = 5
    RULE_varDec = 6
    RULE_className = 7
    RULE_subroutineName = 8
    RULE_varName = 9
    RULE_statements = 10
    RULE_statement = 11
    RULE_letStatement = 12
    RULE_ifStatement = 13
    RULE_whileStatement = 14
    RULE_doStatement = 15
    RULE_returnStatement = 16
    RULE_expression = 17
    RULE_term = 18
    RULE_subroutineCall = 19
    RULE_expressionList = 20
    RULE_op = 21
    RULE_keywordConstant = 22
    RULE_unaryOp = 23

    ruleNames =  [ "classStructure", "classVarDec", "varType", "subroutineDec", 
                   "parameterList", "subroutineBody", "varDec", "className", 
                   "subroutineName", "varName", "statements", "statement", 
                   "letStatement", "ifStatement", "whileStatement", "doStatement", 
                   "returnStatement", "expression", "term", "subroutineCall", 
                   "expressionList", "op", "keywordConstant", "unaryOp" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    INTEGERCONSTANT=41
    STRINGCONSTANT=42
    IDENTIFIER=43
    WS=44
    COMMENT=45
    LINE_COMMENT=46

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ClassStructureContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def className(self):
            return self.getTypedRuleContext(jackParser.ClassNameContext,0)


        def classVarDec(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.ClassVarDecContext)
            else:
                return self.getTypedRuleContext(jackParser.ClassVarDecContext,i)


        def subroutineDec(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.SubroutineDecContext)
            else:
                return self.getTypedRuleContext(jackParser.SubroutineDecContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_classStructure

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassStructure" ):
                listener.enterClassStructure(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassStructure" ):
                listener.exitClassStructure(self)




    def classStructure(self):

        localctx = jackParser.ClassStructureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_classStructure)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(jackParser.T__0)
            self.state = 49
            self.className()
            self.state = 50
            self.match(jackParser.T__1)
            self.state = 54
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==jackParser.T__3 or _la==jackParser.T__4:
                self.state = 51
                self.classVarDec()
                self.state = 56
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 60
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__10) | (1 << jackParser.T__11) | (1 << jackParser.T__12))) != 0):
                self.state = 57
                self.subroutineDec()
                self.state = 62
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 63
            self.match(jackParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassVarDecContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varType(self):
            return self.getTypedRuleContext(jackParser.VarTypeContext,0)


        def varName(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.VarNameContext)
            else:
                return self.getTypedRuleContext(jackParser.VarNameContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_classVarDec

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassVarDec" ):
                listener.enterClassVarDec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassVarDec" ):
                listener.exitClassVarDec(self)




    def classVarDec(self):

        localctx = jackParser.ClassVarDecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_classVarDec)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 65
            _la = self._input.LA(1)
            if not(_la==jackParser.T__3 or _la==jackParser.T__4):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 66
            self.varType()
            self.state = 67
            self.varName()
            self.state = 72
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==jackParser.T__5:
                self.state = 68
                self.match(jackParser.T__5)
                self.state = 69
                self.varName()
                self.state = 74
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 75
            self.match(jackParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def className(self):
            return self.getTypedRuleContext(jackParser.ClassNameContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_varType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarType" ):
                listener.enterVarType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarType" ):
                listener.exitVarType(self)




    def varType(self):

        localctx = jackParser.VarTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_varType)
        try:
            self.state = 81
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [jackParser.T__7]:
                self.enterOuterAlt(localctx, 1)
                self.state = 77
                self.match(jackParser.T__7)
                pass
            elif token in [jackParser.T__8]:
                self.enterOuterAlt(localctx, 2)
                self.state = 78
                self.match(jackParser.T__8)
                pass
            elif token in [jackParser.T__9]:
                self.enterOuterAlt(localctx, 3)
                self.state = 79
                self.match(jackParser.T__9)
                pass
            elif token in [jackParser.IDENTIFIER]:
                self.enterOuterAlt(localctx, 4)
                self.state = 80
                self.className()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SubroutineDecContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subroutineName(self):
            return self.getTypedRuleContext(jackParser.SubroutineNameContext,0)


        def parameterList(self):
            return self.getTypedRuleContext(jackParser.ParameterListContext,0)


        def subroutineBody(self):
            return self.getTypedRuleContext(jackParser.SubroutineBodyContext,0)


        def varType(self):
            return self.getTypedRuleContext(jackParser.VarTypeContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_subroutineDec

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubroutineDec" ):
                listener.enterSubroutineDec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubroutineDec" ):
                listener.exitSubroutineDec(self)




    def subroutineDec(self):

        localctx = jackParser.SubroutineDecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_subroutineDec)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__10) | (1 << jackParser.T__11) | (1 << jackParser.T__12))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 86
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [jackParser.T__13]:
                self.state = 84
                self.match(jackParser.T__13)
                pass
            elif token in [jackParser.T__7, jackParser.T__8, jackParser.T__9, jackParser.IDENTIFIER]:
                self.state = 85
                self.varType()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 88
            self.subroutineName()
            self.state = 89
            self.match(jackParser.T__14)
            self.state = 90
            self.parameterList()
            self.state = 91
            self.match(jackParser.T__15)
            self.state = 92
            self.subroutineBody()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varType(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.VarTypeContext)
            else:
                return self.getTypedRuleContext(jackParser.VarTypeContext,i)


        def varName(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.VarNameContext)
            else:
                return self.getTypedRuleContext(jackParser.VarNameContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_parameterList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameterList" ):
                listener.enterParameterList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameterList" ):
                listener.exitParameterList(self)




    def parameterList(self):

        localctx = jackParser.ParameterListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_parameterList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 106
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__7) | (1 << jackParser.T__8) | (1 << jackParser.T__9) | (1 << jackParser.IDENTIFIER))) != 0):
                self.state = 94
                self.varType()
                self.state = 95
                self.varName()
                self.state = 103
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==jackParser.T__5:
                    self.state = 97
                    self.match(jackParser.T__5)
                    self.state = 98
                    self.varType()
                    self.state = 99
                    self.varName()
                    self.state = 105
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SubroutineBodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statements(self):
            return self.getTypedRuleContext(jackParser.StatementsContext,0)


        def varDec(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.VarDecContext)
            else:
                return self.getTypedRuleContext(jackParser.VarDecContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_subroutineBody

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubroutineBody" ):
                listener.enterSubroutineBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubroutineBody" ):
                listener.exitSubroutineBody(self)




    def subroutineBody(self):

        localctx = jackParser.SubroutineBodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_subroutineBody)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            self.match(jackParser.T__1)
            self.state = 112
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 109
                    self.varDec() 
                self.state = 114
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

            self.state = 115
            self.statements()
            self.state = 116
            self.match(jackParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarDecContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varType(self):
            return self.getTypedRuleContext(jackParser.VarTypeContext,0)


        def varName(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.VarNameContext)
            else:
                return self.getTypedRuleContext(jackParser.VarNameContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_varDec

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarDec" ):
                listener.enterVarDec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarDec" ):
                listener.exitVarDec(self)




    def varDec(self):

        localctx = jackParser.VarDecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_varDec)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 118
            self.match(jackParser.T__16)
            self.state = 119
            self.varType()
            self.state = 120
            self.varName()
            self.state = 125
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==jackParser.T__5:
                self.state = 121
                self.match(jackParser.T__5)
                self.state = 122
                self.varName()
                self.state = 127
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 128
            self.match(jackParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassNameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENTIFIER(self):
            return self.getToken(jackParser.IDENTIFIER, 0)

        def getRuleIndex(self):
            return jackParser.RULE_className

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassName" ):
                listener.enterClassName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassName" ):
                listener.exitClassName(self)




    def className(self):

        localctx = jackParser.ClassNameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_className)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            self.match(jackParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SubroutineNameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENTIFIER(self):
            return self.getToken(jackParser.IDENTIFIER, 0)

        def getRuleIndex(self):
            return jackParser.RULE_subroutineName

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubroutineName" ):
                listener.enterSubroutineName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubroutineName" ):
                listener.exitSubroutineName(self)




    def subroutineName(self):

        localctx = jackParser.SubroutineNameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_subroutineName)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(jackParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarNameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENTIFIER(self):
            return self.getToken(jackParser.IDENTIFIER, 0)

        def getRuleIndex(self):
            return jackParser.RULE_varName

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarName" ):
                listener.enterVarName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarName" ):
                listener.exitVarName(self)




    def varName(self):

        localctx = jackParser.VarNameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_varName)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 134
            self.match(jackParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.StatementContext)
            else:
                return self.getTypedRuleContext(jackParser.StatementContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_statements

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatements" ):
                listener.enterStatements(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatements" ):
                listener.exitStatements(self)




    def statements(self):

        localctx = jackParser.StatementsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_statements)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 139
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__16) | (1 << jackParser.T__17) | (1 << jackParser.T__21) | (1 << jackParser.T__23) | (1 << jackParser.T__24) | (1 << jackParser.T__25))) != 0):
                self.state = 136
                self.statement()
                self.state = 141
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def letStatement(self):
            return self.getTypedRuleContext(jackParser.LetStatementContext,0)


        def ifStatement(self):
            return self.getTypedRuleContext(jackParser.IfStatementContext,0)


        def whileStatement(self):
            return self.getTypedRuleContext(jackParser.WhileStatementContext,0)


        def doStatement(self):
            return self.getTypedRuleContext(jackParser.DoStatementContext,0)


        def returnStatement(self):
            return self.getTypedRuleContext(jackParser.ReturnStatementContext,0)


        def varDec(self):
            return self.getTypedRuleContext(jackParser.VarDecContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = jackParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 148
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [jackParser.T__17]:
                self.state = 142
                self.letStatement()
                pass
            elif token in [jackParser.T__21]:
                self.state = 143
                self.ifStatement()
                pass
            elif token in [jackParser.T__23]:
                self.state = 144
                self.whileStatement()
                pass
            elif token in [jackParser.T__24]:
                self.state = 145
                self.doStatement()
                pass
            elif token in [jackParser.T__25]:
                self.state = 146
                self.returnStatement()
                pass
            elif token in [jackParser.T__16]:
                self.state = 147
                self.varDec()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LetStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varName(self):
            return self.getTypedRuleContext(jackParser.VarNameContext,0)


        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(jackParser.ExpressionContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_letStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLetStatement" ):
                listener.enterLetStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLetStatement" ):
                listener.exitLetStatement(self)




    def letStatement(self):

        localctx = jackParser.LetStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_letStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 150
            self.match(jackParser.T__17)
            self.state = 151
            self.varName()
            self.state = 156
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==jackParser.T__18:
                self.state = 152
                self.match(jackParser.T__18)
                self.state = 153
                self.expression()
                self.state = 154
                self.match(jackParser.T__19)


            self.state = 158
            self.match(jackParser.T__20)
            self.state = 159
            self.expression()
            self.state = 160
            self.match(jackParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(jackParser.ExpressionContext,0)


        def statements(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.StatementsContext)
            else:
                return self.getTypedRuleContext(jackParser.StatementsContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_ifStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfStatement" ):
                listener.enterIfStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfStatement" ):
                listener.exitIfStatement(self)




    def ifStatement(self):

        localctx = jackParser.IfStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_ifStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 162
            self.match(jackParser.T__21)
            self.state = 163
            self.match(jackParser.T__14)
            self.state = 164
            self.expression()
            self.state = 165
            self.match(jackParser.T__15)
            self.state = 166
            self.match(jackParser.T__1)
            self.state = 167
            self.statements()
            self.state = 168
            self.match(jackParser.T__2)
            self.state = 174
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==jackParser.T__22:
                self.state = 169
                self.match(jackParser.T__22)
                self.state = 170
                self.match(jackParser.T__1)
                self.state = 171
                self.statements()
                self.state = 172
                self.match(jackParser.T__2)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhileStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(jackParser.ExpressionContext,0)


        def statements(self):
            return self.getTypedRuleContext(jackParser.StatementsContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_whileStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhileStatement" ):
                listener.enterWhileStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhileStatement" ):
                listener.exitWhileStatement(self)




    def whileStatement(self):

        localctx = jackParser.WhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_whileStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 176
            self.match(jackParser.T__23)
            self.state = 177
            self.match(jackParser.T__14)
            self.state = 178
            self.expression()
            self.state = 179
            self.match(jackParser.T__15)
            self.state = 180
            self.match(jackParser.T__1)
            self.state = 181
            self.statements()
            self.state = 182
            self.match(jackParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DoStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subroutineCall(self):
            return self.getTypedRuleContext(jackParser.SubroutineCallContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_doStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDoStatement" ):
                listener.enterDoStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDoStatement" ):
                listener.exitDoStatement(self)




    def doStatement(self):

        localctx = jackParser.DoStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_doStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 184
            self.match(jackParser.T__24)
            self.state = 185
            self.subroutineCall()
            self.state = 186
            self.match(jackParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReturnStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(jackParser.ExpressionContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_returnStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReturnStatement" ):
                listener.enterReturnStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReturnStatement" ):
                listener.exitReturnStatement(self)




    def returnStatement(self):

        localctx = jackParser.ReturnStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_returnStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 188
            self.match(jackParser.T__25)
            self.state = 190
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__14) | (1 << jackParser.T__28) | (1 << jackParser.T__35) | (1 << jackParser.T__36) | (1 << jackParser.T__37) | (1 << jackParser.T__38) | (1 << jackParser.T__39) | (1 << jackParser.INTEGERCONSTANT) | (1 << jackParser.STRINGCONSTANT) | (1 << jackParser.IDENTIFIER))) != 0):
                self.state = 189
                self.expression()


            self.state = 192
            self.match(jackParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.TermContext)
            else:
                return self.getTypedRuleContext(jackParser.TermContext,i)


        def op(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.OpContext)
            else:
                return self.getTypedRuleContext(jackParser.OpContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)




    def expression(self):

        localctx = jackParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_expression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self.term()
            self.state = 200
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__20) | (1 << jackParser.T__27) | (1 << jackParser.T__28) | (1 << jackParser.T__29) | (1 << jackParser.T__30) | (1 << jackParser.T__31) | (1 << jackParser.T__32) | (1 << jackParser.T__33) | (1 << jackParser.T__34))) != 0):
                self.state = 195
                self.op()
                self.state = 196
                self.term()
                self.state = 202
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGERCONSTANT(self):
            return self.getToken(jackParser.INTEGERCONSTANT, 0)

        def STRINGCONSTANT(self):
            return self.getToken(jackParser.STRINGCONSTANT, 0)

        def keywordConstant(self):
            return self.getTypedRuleContext(jackParser.KeywordConstantContext,0)


        def varName(self):
            return self.getTypedRuleContext(jackParser.VarNameContext,0)


        def expression(self):
            return self.getTypedRuleContext(jackParser.ExpressionContext,0)


        def subroutineCall(self):
            return self.getTypedRuleContext(jackParser.SubroutineCallContext,0)


        def unaryOp(self):
            return self.getTypedRuleContext(jackParser.UnaryOpContext,0)


        def term(self):
            return self.getTypedRuleContext(jackParser.TermContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)




    def term(self):

        localctx = jackParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_term)
        try:
            self.state = 220
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 203
                self.match(jackParser.INTEGERCONSTANT)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 204
                self.match(jackParser.STRINGCONSTANT)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 205
                self.keywordConstant()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 206
                self.varName()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 207
                self.varName()
                self.state = 208
                self.match(jackParser.T__18)
                self.state = 209
                self.expression()
                self.state = 210
                self.match(jackParser.T__19)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 212
                self.subroutineCall()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 213
                self.match(jackParser.T__14)
                self.state = 214
                self.expression()
                self.state = 215
                self.match(jackParser.T__15)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 217
                self.unaryOp()
                self.state = 218
                self.term()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SubroutineCallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subroutineName(self):
            return self.getTypedRuleContext(jackParser.SubroutineNameContext,0)


        def expressionList(self):
            return self.getTypedRuleContext(jackParser.ExpressionListContext,0)


        def className(self):
            return self.getTypedRuleContext(jackParser.ClassNameContext,0)


        def varName(self):
            return self.getTypedRuleContext(jackParser.VarNameContext,0)


        def getRuleIndex(self):
            return jackParser.RULE_subroutineCall

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubroutineCall" ):
                listener.enterSubroutineCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubroutineCall" ):
                listener.exitSubroutineCall(self)




    def subroutineCall(self):

        localctx = jackParser.SubroutineCallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_subroutineCall)
        try:
            self.state = 237
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 222
                self.subroutineName()
                self.state = 223
                self.match(jackParser.T__14)
                self.state = 224
                self.expressionList()
                self.state = 225
                self.match(jackParser.T__15)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 229
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,16,self._ctx)
                if la_ == 1:
                    self.state = 227
                    self.className()
                    pass

                elif la_ == 2:
                    self.state = 228
                    self.varName()
                    pass


                self.state = 231
                self.match(jackParser.T__26)
                self.state = 232
                self.subroutineName()
                self.state = 233
                self.match(jackParser.T__14)
                self.state = 234
                self.expressionList()
                self.state = 235
                self.match(jackParser.T__15)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(jackParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(jackParser.ExpressionContext,i)


        def getRuleIndex(self):
            return jackParser.RULE_expressionList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpressionList" ):
                listener.enterExpressionList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpressionList" ):
                listener.exitExpressionList(self)




    def expressionList(self):

        localctx = jackParser.ExpressionListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_expressionList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 247
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__14) | (1 << jackParser.T__28) | (1 << jackParser.T__35) | (1 << jackParser.T__36) | (1 << jackParser.T__37) | (1 << jackParser.T__38) | (1 << jackParser.T__39) | (1 << jackParser.INTEGERCONSTANT) | (1 << jackParser.STRINGCONSTANT) | (1 << jackParser.IDENTIFIER))) != 0):
                self.state = 239
                self.expression()
                self.state = 244
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==jackParser.T__5:
                    self.state = 240
                    self.match(jackParser.T__5)
                    self.state = 241
                    self.expression()
                    self.state = 246
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return jackParser.RULE_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOp" ):
                listener.enterOp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOp" ):
                listener.exitOp(self)




    def op(self):

        localctx = jackParser.OpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 249
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__20) | (1 << jackParser.T__27) | (1 << jackParser.T__28) | (1 << jackParser.T__29) | (1 << jackParser.T__30) | (1 << jackParser.T__31) | (1 << jackParser.T__32) | (1 << jackParser.T__33) | (1 << jackParser.T__34))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class KeywordConstantContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return jackParser.RULE_keywordConstant

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterKeywordConstant" ):
                listener.enterKeywordConstant(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitKeywordConstant" ):
                listener.exitKeywordConstant(self)




    def keywordConstant(self):

        localctx = jackParser.KeywordConstantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_keywordConstant)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 251
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << jackParser.T__35) | (1 << jackParser.T__36) | (1 << jackParser.T__37) | (1 << jackParser.T__38))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnaryOpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return jackParser.RULE_unaryOp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnaryOp" ):
                listener.enterUnaryOp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnaryOp" ):
                listener.exitUnaryOp(self)




    def unaryOp(self):

        localctx = jackParser.UnaryOpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_unaryOp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 253
            _la = self._input.LA(1)
            if not(_la==jackParser.T__28 or _la==jackParser.T__39):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





