grammar jack;

//Program structure
classStructure   : 'class' className '{' classVarDec* subroutineDec* '}';
classVarDec :   ('static' | 'field') varType varName (',' varName)* ';';
varType    :   'int' | 'char' | 'boolean' | className;
subroutineDec   :   ('constructor' | 'function' | 'method')
                    ('void' | varType) subroutineName '(' parameterList ')'
                    subroutineBody;
parameterList   :   ((varType varName) (',' varType varName)*)?;
subroutineBody  :   '{' varDec* statements '}';
varDec  :   'var' varType varName (',' varName)* ';';
className   :   IDENTIFIER;
subroutineName  :   IDENTIFIER;
varName :   IDENTIFIER;

//Statements
statements  :   (statement)*;
statement   :   (letStatement | ifStatement | whileStatement | doStatement | returnStatement | varDec);
letStatement    :   'let' varName ('[' expression ']')? '=' expression ';';
ifStatement :   'if' '(' expression ')' '{' statements '}' ('else' '{' statements '}')?;
whileStatement  :   'while' '(' expression ')' '{' statements '}';
doStatement :   'do'    subroutineCall ';';
returnStatement :   'return' expression? ';';

//Expressions
expression  :   term (op term)*;
term    :   INTEGERCONSTANT | STRINGCONSTANT | keywordConstant | varName | varName '[' expression ']' |
            subroutineCall | '(' expression ')' | unaryOp term;
subroutineCall  :   subroutineName '(' expressionList ')' |
                    (className | varName) '.' subroutineName '(' expressionList ')';
expressionList  :   (expression (',' expression)*)?;


// Lexer rules
op  :   ('+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '=');

keywordConstant :   ('true' | 'false' | 'null' | 'this');

unaryOp :   ('-' | '~');

fragment DIGIT  : [0-9];
INTEGERCONSTANT : DIGIT+;

fragment LOWERCASE  : [a-z];
fragment UPPERCASE  : [A-Z];

STRINGCONSTANT  : '"' (LOWERCASE | UPPERCASE | '_' | DIGIT | ~[\\"\r\n])* '"';

IDENTIFIER  : (LOWERCASE | UPPERCASE | '_')+ (DIGIT | LOWERCASE | UPPERCASE | '_')*;

WS : [ \t\r\n]+ -> skip ; // Define whitespace rule, toss it out

COMMENT
    : '/*' .*? '*/' -> skip
;

LINE_COMMENT
    : '//' ~[\r\n]* -> skip
;