# Generated from .\jack.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .jackParser import jackParser
else:
    from jackParser import jackParser

# This class defines a complete listener for a parse tree produced by jackParser.
class jackListener(ParseTreeListener):

    # Enter a parse tree produced by jackParser#classStructure.
    def enterClassStructure(self, ctx:jackParser.ClassStructureContext):
        pass

    # Exit a parse tree produced by jackParser#classStructure.
    def exitClassStructure(self, ctx:jackParser.ClassStructureContext):
        pass


    # Enter a parse tree produced by jackParser#classVarDec.
    def enterClassVarDec(self, ctx:jackParser.ClassVarDecContext):
        pass

    # Exit a parse tree produced by jackParser#classVarDec.
    def exitClassVarDec(self, ctx:jackParser.ClassVarDecContext):
        pass


    # Enter a parse tree produced by jackParser#varType.
    def enterVarType(self, ctx:jackParser.VarTypeContext):
        pass

    # Exit a parse tree produced by jackParser#varType.
    def exitVarType(self, ctx:jackParser.VarTypeContext):
        pass


    # Enter a parse tree produced by jackParser#subroutineDec.
    def enterSubroutineDec(self, ctx:jackParser.SubroutineDecContext):
        pass

    # Exit a parse tree produced by jackParser#subroutineDec.
    def exitSubroutineDec(self, ctx:jackParser.SubroutineDecContext):
        pass


    # Enter a parse tree produced by jackParser#parameterList.
    def enterParameterList(self, ctx:jackParser.ParameterListContext):
        pass

    # Exit a parse tree produced by jackParser#parameterList.
    def exitParameterList(self, ctx:jackParser.ParameterListContext):
        pass


    # Enter a parse tree produced by jackParser#subroutineBody.
    def enterSubroutineBody(self, ctx:jackParser.SubroutineBodyContext):
        pass

    # Exit a parse tree produced by jackParser#subroutineBody.
    def exitSubroutineBody(self, ctx:jackParser.SubroutineBodyContext):
        pass


    # Enter a parse tree produced by jackParser#varDec.
    def enterVarDec(self, ctx:jackParser.VarDecContext):
        pass

    # Exit a parse tree produced by jackParser#varDec.
    def exitVarDec(self, ctx:jackParser.VarDecContext):
        pass


    # Enter a parse tree produced by jackParser#className.
    def enterClassName(self, ctx:jackParser.ClassNameContext):
        pass

    # Exit a parse tree produced by jackParser#className.
    def exitClassName(self, ctx:jackParser.ClassNameContext):
        pass


    # Enter a parse tree produced by jackParser#subroutineName.
    def enterSubroutineName(self, ctx:jackParser.SubroutineNameContext):
        pass

    # Exit a parse tree produced by jackParser#subroutineName.
    def exitSubroutineName(self, ctx:jackParser.SubroutineNameContext):
        pass


    # Enter a parse tree produced by jackParser#varName.
    def enterVarName(self, ctx:jackParser.VarNameContext):
        pass

    # Exit a parse tree produced by jackParser#varName.
    def exitVarName(self, ctx:jackParser.VarNameContext):
        pass


    # Enter a parse tree produced by jackParser#statements.
    def enterStatements(self, ctx:jackParser.StatementsContext):
        pass

    # Exit a parse tree produced by jackParser#statements.
    def exitStatements(self, ctx:jackParser.StatementsContext):
        pass


    # Enter a parse tree produced by jackParser#statement.
    def enterStatement(self, ctx:jackParser.StatementContext):
        pass

    # Exit a parse tree produced by jackParser#statement.
    def exitStatement(self, ctx:jackParser.StatementContext):
        pass


    # Enter a parse tree produced by jackParser#letStatement.
    def enterLetStatement(self, ctx:jackParser.LetStatementContext):
        pass

    # Exit a parse tree produced by jackParser#letStatement.
    def exitLetStatement(self, ctx:jackParser.LetStatementContext):
        pass


    # Enter a parse tree produced by jackParser#ifStatement.
    def enterIfStatement(self, ctx:jackParser.IfStatementContext):
        pass

    # Exit a parse tree produced by jackParser#ifStatement.
    def exitIfStatement(self, ctx:jackParser.IfStatementContext):
        pass


    # Enter a parse tree produced by jackParser#whileStatement.
    def enterWhileStatement(self, ctx:jackParser.WhileStatementContext):
        pass

    # Exit a parse tree produced by jackParser#whileStatement.
    def exitWhileStatement(self, ctx:jackParser.WhileStatementContext):
        pass


    # Enter a parse tree produced by jackParser#doStatement.
    def enterDoStatement(self, ctx:jackParser.DoStatementContext):
        pass

    # Exit a parse tree produced by jackParser#doStatement.
    def exitDoStatement(self, ctx:jackParser.DoStatementContext):
        pass


    # Enter a parse tree produced by jackParser#returnStatement.
    def enterReturnStatement(self, ctx:jackParser.ReturnStatementContext):
        pass

    # Exit a parse tree produced by jackParser#returnStatement.
    def exitReturnStatement(self, ctx:jackParser.ReturnStatementContext):
        pass


    # Enter a parse tree produced by jackParser#expression.
    def enterExpression(self, ctx:jackParser.ExpressionContext):
        pass

    # Exit a parse tree produced by jackParser#expression.
    def exitExpression(self, ctx:jackParser.ExpressionContext):
        pass


    # Enter a parse tree produced by jackParser#term.
    def enterTerm(self, ctx:jackParser.TermContext):
        pass

    # Exit a parse tree produced by jackParser#term.
    def exitTerm(self, ctx:jackParser.TermContext):
        pass


    # Enter a parse tree produced by jackParser#subroutineCall.
    def enterSubroutineCall(self, ctx:jackParser.SubroutineCallContext):
        pass

    # Exit a parse tree produced by jackParser#subroutineCall.
    def exitSubroutineCall(self, ctx:jackParser.SubroutineCallContext):
        pass


    # Enter a parse tree produced by jackParser#expressionList.
    def enterExpressionList(self, ctx:jackParser.ExpressionListContext):
        pass

    # Exit a parse tree produced by jackParser#expressionList.
    def exitExpressionList(self, ctx:jackParser.ExpressionListContext):
        pass


    # Enter a parse tree produced by jackParser#op.
    def enterOp(self, ctx:jackParser.OpContext):
        pass

    # Exit a parse tree produced by jackParser#op.
    def exitOp(self, ctx:jackParser.OpContext):
        pass


    # Enter a parse tree produced by jackParser#keywordConstant.
    def enterKeywordConstant(self, ctx:jackParser.KeywordConstantContext):
        pass

    # Exit a parse tree produced by jackParser#keywordConstant.
    def exitKeywordConstant(self, ctx:jackParser.KeywordConstantContext):
        pass


    # Enter a parse tree produced by jackParser#unaryOp.
    def enterUnaryOp(self, ctx:jackParser.UnaryOpContext):
        pass

    # Exit a parse tree produced by jackParser#unaryOp.
    def exitUnaryOp(self, ctx:jackParser.UnaryOpContext):
        pass



del jackParser